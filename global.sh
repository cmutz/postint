#!/bin/bash
export PATH_BASH="/bin/bash"
export PATH_CP="/bin/cp"
export PATH_INIT_SCRIPT="INIT_SCRIPT"
export PATH_END_SCRIPT="END_SCRIPT"
export PATH_CONFIGURATION="CONFIGURATION"
export PATH_LIBRARY="LIBRARY"
export PATH_WGET="/usr/bin/wget"
export LOGFILE=./log.`date +%s`.out
export SCRIPT_TYPE=$1
export CONFIGURATION_CUSTOM=$2
export INSTALL_AUTO=$3
export MAIL="infrastructure@whoople.fr"
export VERSION_OWNCLOUD="9.0.1"
